from Project import app


@app.template_filter()
def currency_format(value):
    value = float(value) / 100
    return "£{:,.2f}".format(value)


if __name__ == '__main__':
    app.run(debug=True)
