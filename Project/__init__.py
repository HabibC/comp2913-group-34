from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_mail import Mail

app = Flask(__name__)

#Sets up database
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, "database.db")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
Migrate(app, db)

#For emails
app.config['MAIL_SERVER']='smtp.office365.com'
app.config['MAIL_PORT'] = 587
app.config['MAIL_USERNAME'] = 'ArenaCinemaLeeds@outlook.com'
app.config['MAIL_PASSWORD'] = 'SoftwareProjectGroup34'
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USE_SSL'] = False
app.config['MAIL_DEFAULT_SENDER'] = ("Arena Cinema", "ArenaCinemaLeeds@outlook.com")

email = Mail(app)

#Allows forms to function
SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY

#Sets up login manager
login_manager = LoginManager(app)
login_manager.login_view = "users.login"
login_manager.login_message_category = "danger"

from Project.business.views import business
from Project.users.views import users
from Project.owner.views import owner

app.register_blueprint(business)
app.register_blueprint(users)
app.register_blueprint(owner)

from Project.models import User
db.create_all()
