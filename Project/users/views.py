from flask import render_template, url_for, flash, redirect, request, Blueprint
from flask_login import login_user, current_user, logout_user, login_required
from Project import db, email, app
from Project.models import User, Ticket, Movie, Screening, Sale, CardDetails
from Project.users.forms import RegistrationForm, LoginForm, AddTicketForm, PickSeatForm, SearchForm
from Project.business.forms import AddCardPaymentForm
import qrcode
import json
from reportlab.pdfgen import canvas
from flask_mail import Message
import os

users = Blueprint("users", __name__)


def currency_format(value):
    value = float(value) / 100
    return "£{:,.2f}".format(value)


@users.route('/list', methods=['GET', 'POST'])
def display_movies():
    form = SearchForm()
    list_movies = Movie.query.all()
    list_screenings = Screening.query.all()
    if form.validate_on_submit():
        search = form.search.data
        list_movies = Movie.query.filter(Movie.name.like("%" + search + "%"))
        return render_template('list.html', list_movies=list_movies, list_screenings=list_screenings, form=form)
    return render_template('list.html', list_movies=list_movies, list_screenings=list_screenings, form=form)


@users.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Logged out", "info")
    return redirect("/list")


@users.route("/register", methods=['GET', 'POST'])
def register():
    form = RegistrationForm()

    if form.validate_on_submit():
        if form.password.data == form.confirm_password.data:
            email = form.email.data
            email_exists = User.query.filter_by(
                email=email).first() is not None
            if email_exists:
                flash("That email is already used for another account", "danger")
            username = form.username.data
            username_exists = User.query.filter_by(
                username=username).first() is not None
            if username_exists:
                flash("That username is already used for another account", "danger")
            if username_exists or email_exists:
                return render_template("register.html", form=form)

            user = User(email=form.email.data,
                        username=form.username.data,
                        password=form.password.data, is_staff=False, is_manager=False)
            db.session.add(user)
            db.session.commit()

            flash("You have been registered", "success")
            return redirect(url_for("users.login"))
        else:
            flash("Passwords did not match", "danger")
    return render_template("register.html", form=form)


@users.route("/login", methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is not None and user.check_password(form.password.data):
            login_user(user)
            flash("Logged in", "success")
            next = request.args.get('next')
            if next:
                return redirect(next)
            else:
                if current_user.is_staff:
                    return redirect(url_for("business.display_movies"))
                else:
                    return redirect(url_for("users.display_movies"))
        else:
            flash("Login error", "danger")
    return render_template("login.html", form=form)


@users.route('/buy_ticket_online/<int:screening_id>/<string:seat>', methods=['GET', 'POST'])
def buy_ticket(screening_id, seat):
    form = AddTicketForm()
    screening = Screening.query.get(screening_id)
    movie_id = Screening.query.with_entities(
        Screening.movie_id).filter_by(id=screening_id).first().movie_id
    movie = Movie.query.get(movie_id)
    price = movie.price_as_pence
    is_vip = 0
    if int(seat) >= 1 and int(seat) <= 20:
        is_vip = 1
        price += 200
    if form.validate_on_submit():
        age = form.age.data
        certificate = movie.certificate
        can_watch = True
        if ((certificate == "12" or certificate == "12A") and int(age) < 12):
            can_watch = False
        if(certificate == "15" and int(age) < 15):
            can_watch = False
        if(certificate == "18" and int(age) < 18):
            can_watch = False
        if can_watch:
            email = form.email.data
            first_name = form.first_name.data
            last_name = form.last_name.data
            new_ticket = Ticket(screening_id, seat, email,
                                first_name, last_name, age)
            db.session.add(new_ticket)
            db.session.commit()

            has_discount = 0
            if (int(form.age.data) >= 64 or int(form.age.data) <= 10):
                has_discount = 1

            form.email.data = ""
            form.age.data = ""
            form.first_name.data = ""
            form.last_name.data = ""
            return redirect(url_for('.make_card_payment', screening_id=screening_id, seat=seat, has_discount=has_discount))
        else:
            flash('Too young to watch movie: minimum 13 required', "danger")

    return render_template('buy_ticket_online.html', form=form, movie=movie, price=price, screening=screening, is_vip=is_vip)


@users.route('/card_payment/<int:screening_id>/<string:seat>/<int:has_discount>', methods=['GET', 'POST'])
def make_card_payment(screening_id, seat, has_discount):
    form = AddCardPaymentForm()
    if current_user.is_authenticated:
        details = CardDetails.query.filter_by(
            user_id=current_user.id).first()
        if details:
            flash("Saved details entered", "info")
            form.name.data = details.name
            form.card_number.data = details.card_number
            form.expiration.data = details.expiration
            form.security_code.data = details.security_code
            form.address.data = details.address
            form.town_city.data = details.town_city
            form.postcode.data = details.postcode
            form.country.data = details.country
    if form.validate_on_submit():
        name = form.name.data
        card_number = form.card_number.data
        expiration = form.expiration.data
        security_code = form.security_code.data
        address = form.address.data
        town_city = form.town_city.data
        postcode = form.postcode.data
        country = form.country.data

        remember = form.remember_me.data
        if remember == True:
            new_details = CardDetails(current_user.id, card_number, name,
                                      expiration, security_code, address, town_city, postcode, country)
            db.session.add(new_details)
            db.session.commit()

        form.name.data = ""
        form.card_number.data = ""
        form.expiration.data = ""
        form.security_code.data = ""
        form.address.data = ""
        form.town_city.data = ""
        form.postcode.data = ""
        form.country.data = ""

        is_vip = False
        if int(seat) >= 0 and int(seat) <= 20:
            is_vip = True
        movie_id = Screening.query.with_entities(
            Screening.movie_id).filter_by(id=screening_id).first().movie_id
        price = int(Movie.query.with_entities(
            Movie.price_as_pence).filter_by(id=movie_id).first().price_as_pence)
        if is_vip:
            price += 200
        if has_discount == 1:
            price = int(price * 0.9)

        new_sale = Sale(screening_id, seat, price)

        db.session.add(new_sale)
        db.session.commit()
        return redirect(url_for("users.generate_pdf_send"))

    return render_template("card_payment.html", form=form)


@users.route("/ticket_qrcode_pdf_send")
def generate_pdf_send():
    last_ticket = (Ticket.query.order_by(Ticket.id.desc()).first())
    last_ticket_dict = (last_ticket.__dict__)

    last_ticket_dict.pop('_sa_instance_state')
    ticket_json = json.dumps(last_ticket_dict)

    qrcode_image = qrcode.make(ticket_json)
    image_path = "ticket_qr_" + str(last_ticket_dict["id"]) + ".png"
    qrcode_image.save(image_path)

    screening_id = Ticket.query.with_entities(
        Ticket.screening_id).order_by(Ticket.id.desc()).first().screening_id
    time = str(Screening.query.with_entities(
        Screening.time).filter_by(id=screening_id).first().time)
    date = str(Screening.query.with_entities(
        Screening.date).filter_by(id=screening_id).first().date)
    seat = (Ticket.query.with_entities(
        Ticket.seat).order_by(Ticket.id.desc()).first()).seat
    movie_id = Screening.query.with_entities(
        Screening.movie_id).filter_by(id=screening_id).first().movie_id
    certificate = str(Movie.query.with_entities(
        Movie.certificate).filter_by(id=movie_id).first().certificate)
    email_address = (Ticket.query.with_entities(
        Ticket.email).order_by(Ticket.id.desc()).first()).email
    
    id_required = False
    if certificate == "12A" or certificate == "12" or certificate == "15" or certificate == "18":
        id_required = True
    
    name = Movie.query.with_entities(
        Movie.name).filter_by(id=movie_id).first().name
    
    sale = Sale.query.order_by(Sale.id.desc()).first()
    sale_id = sale.id
    price = sale.price
    tos = sale.time_of_sale

    filename = "ticket_" + str(last_ticket_dict["id"]) + ".pdf"
    pdf = canvas.Canvas(filename)
    pdf.setTitle("ticket_" + str(last_ticket_dict["id"]))

    pdf.setFont("Helvetica", 16)
    pdf.drawCentredString(270, 770, "Arena Cinema")
    pdf.setFont("Helvetica", 12)
    pdf.drawCentredString(270, 755, "Albion Street")
    pdf.setFont("Helvetica", 10)
    pdf.drawCentredString(270, 745, "Leeds")

    pdf.setFont("Courier-Bold", 32)
    pdf.drawCentredString(270, 710, name)
    pdf.setFont("Courier-Bold", 14)
    if not id_required:
        pdf.drawCentredString(270, 690, certificate)
    else:
        pdf.drawString(180, 690, certificate)
        pdf.drawString(210, 690, "(ID may be required)")

    pdf.line(30, 680, 550, 680)

    pdf.setFont("Courier-Bold", 28)
    pdf.drawCentredString(270, 650, date)
    pdf.drawCentredString(270, 620, "at " + time)
    pdf.drawCentredString(270, 590, "SEAT: " + seat)

    pdf.line(30, 580, 550, 580)

    pdf.drawInlineImage(image_path, 140, 270, 285, 285)

    pdf.line(30, 250, 550, 250)

    pdf.setFont("Courier-Bold", 14)
    pdf.drawString(90, 230, "Sale: " + str(sale_id))
    pdf.drawString(180, 230, "Time of sale: " + tos.strftime('%H:%M:%S'))
    pdf.drawString(390, 230, "Price: " + currency_format(price))

    pdf.save()
    path = os.path.join(os.getcwd(), filename)
    msg = Message("Your Arena Cinema ticket", [email_address],
                  "Attached is your ticket to see " + name + " on " + date + " at " + time)
    with app.open_resource(path) as fp:
        msg.attach(filename, "application/pdf", fp.read())

    # Only use when necessary. There is a limit on the number of emails that can be sent
    email.send(msg)
    flash("Payment made - Check your emails for the ticket", "success")
    return redirect(url_for("users.display_movies"))


@users.route('/seats_for_screening/<int:screening_id>', methods=['GET', 'POST'])
def get_seats(screening_id):
    selected_seat = "1"
    form = PickSeatForm()

    list_movies = Movie.query.all()
    list_screenings = Screening.query.all()
    screen = Screening.query.get(screening_id)

    avail_seats = []
    for i in range(1, 101):
        avail_seats.append(str(i))
        if i >= 1 and i <= 20:
            avail_seats[i - 1] += " - VIP"
    # This holds the sold seats in the format of [(1,), (2,), (3,), (4,)]

    sold_seats = Sale.query.with_entities(
        Sale.seat).filter_by(screening_id=screening_id).all()

    # remove the sold seats from the available seats
    for i in range(len(sold_seats)):
        avail_seats[sold_seats[i][0] - 1] = "Sold"

    if form.validate_on_submit():
        if(avail_seats[int(form.seat.data) - 1] == "Sold"):
            flash("Seat not available", "danger")
            return redirect(url_for(".get_seats", screening_id=screening_id))
        else:
            selected_seat = form.seat.data
            return redirect(url_for(".buy_ticket", screening_id=screening_id, seat=selected_seat))

    return render_template("pick_seat.html", form=form, list_movies=list_movies, list_screenings=list_screenings, screen=screen, avail_seats=avail_seats, selected_seat=selected_seat, screening_id=screening_id)
