from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, IntegerField
from wtforms.validators import DataRequired, Email, EqualTo
from wtforms import ValidationError
from wtforms.fields.html5 import SearchField

from Project.models import User


class SearchForm(FlaskForm):
	search = SearchField()


class LoginForm(FlaskForm):
	username = StringField("Username", validators=[DataRequired()])
	password = PasswordField("Password", validators=[DataRequired()])
	submit = SubmitField("Log in")


class RegistrationForm(FlaskForm):
	email = StringField("Email", validators=[DataRequired(), Email()])
	username = StringField("Username", validators=[DataRequired()])
	password = PasswordField("Password", validators=[DataRequired(), EqualTo(
		"confirm_pass", message="Passwords must match")])
	confirm_password = password = PasswordField(
		"Confirm password", validators=[DataRequired()])
	submit = SubmitField("Register")

	def check_email(self, field):
		if User.query.filterBy(email=field.data).first():
			raise ValidationError(
				"Another account is linked to this email address")

	def check_username(self, field):
		if User.query.filterBy(username=field.data).first():
			raise ValidationError("This username is already in use")


class AddTicketForm(FlaskForm):
	screening_id = IntegerField('Screening ID:')
	email = StringField('Email Address:')
	first_name = StringField('First Name:')
	last_name = StringField('Last Name:')
	age = IntegerField('Age:')
	submit = SubmitField('Buy Ticket')


class PickSeatForm(FlaskForm):
	seat = StringField("Seat: ")
	submit = SubmitField('Select Seat')
