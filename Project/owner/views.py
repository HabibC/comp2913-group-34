from Project import app
import matplotlib.pyplot as plt
from flask import render_template, Blueprint, flash
from flask_login import login_required, current_user
from functools import wraps
from datetime import datetime, timedelta
from Project.models import Movie, Screening, Sale
import matplotlib
matplotlib.use('Agg')
owner = Blueprint('owner', __name__)


def currency_format(value):
    value = float(value) / 100
    return "£{:,.2f}".format(value)


def make_autopct(values):
    def my_autopct(pct):
        total = sum(values)
        val = int(round(pct*total/100.0))
        return '{p:.2f}%  ({v})'.format(p=pct, v=currency_format(val))
    return my_autopct


def staff_manager_login_required(role, *args, **kwargs):
    def login_required(func):
        @wraps(func)
        def decorated_view(*args, **kwargs):
            if not current_user.is_authenticated:
                return app.login_manager.unauthorized()
            elif current_user.is_staff != 1:
                flash("Restricted - Staff only", "danger")
                return app.login_manager.unauthorized()
            elif role == "manager":
                if current_user.is_manager != 1:
                    flash("Restricted - Managers only", "danger")
                    return app.login_manager.unauthorized()
            return func(*args, **kwargs)
        return decorated_view
    return login_required


@owner.route("/business/weekly_income")
@staff_manager_login_required("manager")
@login_required
def income():
    current_datetime = datetime.now()
    week_ago = current_datetime - timedelta(weeks=1)
    past_week_sales = Sale.query.filter(Sale.time_of_sale > week_ago).all()
    per_movie_sales = {}
    total_income = 0

    for sale in past_week_sales:
        total_income += sale.price

    for sale in past_week_sales:
        screening_id = sale.screening_id
        movie_id = Screening.query.filter_by(id=screening_id).first().movie_id
        name = Movie.query.filter_by(id=movie_id).first().name
        if name not in per_movie_sales:
            per_movie_sales[name] = sale.price
        else:
            per_movie_income = per_movie_sales[name] + sale.price
            per_movie_sales.update({name: per_movie_income})

    keys = per_movie_sales.keys()
    values = per_movie_sales.values()
    plt.tight_layout()
    plt.pie(values, labels=keys, wedgeprops={
        'edgecolor': 'black'}, shadow=True, autopct=make_autopct(values))
    plt.savefig("Project/static/weekly_income.png", bbox_inches="tight")
    plt.clf()
    return render_template("income.html", total_income=total_income, per_movie_sales=per_movie_sales, current_datetime=current_datetime)


@owner.route("/business/compare")
@staff_manager_login_required("manager")
@login_required
def compare_movies():
    current_datetime = datetime.now()
    current_datetime_string = str(current_datetime)
    week_ago = current_datetime - timedelta(weeks=1)
    movie_sales = {}
    past_week_sales = Sale.query.filter(Sale.time_of_sale > week_ago).all()
    for sale in past_week_sales:
        screening_id = sale.screening_id
        movie_id = Screening.query.filter_by(id=screening_id).first().movie_id
        name = Movie.query.filter_by(id=movie_id).first().name
        if name not in movie_sales:
            movie_sales[name] = 1
        else:
            sold = movie_sales[name] + 1
            movie_sales.update({name: sold})

    movie = movie_sales.keys()
    sales = movie_sales.values()
    plt.bar(movie, sales)
    plt.savefig("Project/static/sales.png", bbox_inches="tight")
    plt.clf()
    return render_template("compare.html", current_datetime_string=current_datetime_string, current_datetime=current_datetime)
