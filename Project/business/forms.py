from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, TextAreaField, IntegerField, BooleanField
from wtforms.fields.core import FloatField
from wtforms.fields.html5 import DateField, TimeField
from wtforms.validators import DataRequired, Email, EqualTo
from wtforms import ValidationError

from Project.models import Movie, User


class AddMovieForm(FlaskForm):
    name = StringField('Movie Name')
    blurb = TextAreaField('Movie Blurb', render_kw={"rows": 4, "cols": 30})
    certificate = StringField('Movie Certificate')
    actors = StringField('Movie Actors')
    price = FloatField('Price')
    image_path = StringField('Poster')
    submit = SubmitField('Add Movie')

    def check_movie(self, field):
        if Movie.query.filter_by(name=field.data).first():
            raise ValidationError('This movie name already exists!')


class AddScreeningForm(FlaskForm):
    time = TimeField('Time')
    date = DateField('Date', format='%Y-%m-%d')
    submit = SubmitField('Add Screening')


class AddCardPaymentForm(FlaskForm):
    name = StringField("Name")
    card_number = StringField("Card number")
    security_code = StringField("Security code")
    expiration = StringField("Expiration date")
    address = StringField("Address")
    town_city = StringField("Town/City")
    postcode = StringField("Post Code")
    country = StringField("Country")
    remember_me = BooleanField("Remember Details?", default=False)


class AddCashPaymentForm(FlaskForm):
    amount_paid = FloatField("Amount Paid")


class AddCardPaymentForm(FlaskForm):
    name = StringField("Name")
    card_number = StringField("Card number")
    security_code = StringField("Security code")
    expiration = StringField("Expiration date")
    address = StringField("Address")
    town_city = StringField("Town/City")
    postcode = StringField("Post Code")
    country = StringField("Country")
    remember_me = BooleanField("Remember Details?", default=False)


class AddTicketForm(FlaskForm):
    screening_id = IntegerField('Screening ID')
    email = StringField('Email Address')
    first_name = StringField('First Name')
    last_name = StringField('Last Name')
    age = IntegerField('Age')
    pay_by_card = BooleanField("Pay by card", default=False)
    submit = SubmitField('Buy Ticket')


class LoginForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])
    submit = SubmitField("Log in")


class RegistrationForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Email()])
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired(), EqualTo(
        "confirm_pass", message="Passwords must match")])
    confirm_password = password = PasswordField(
        "Confirm password", validators=[DataRequired()])
    is_manager = BooleanField("Manager account?", default=False)
    submit = SubmitField("Register")

    def check_email(self, field):
        if User.query.filterBy(email=field.data).first():
            raise ValidationError(
                "Another account is linked to this email address")

    def check_username(self, field):
        if User.query.filterBy(username=field.data).first():
            raise ValidationError("This username is already in use")
