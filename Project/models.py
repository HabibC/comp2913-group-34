from datetime import datetime
from Project import db, login_manager
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin


@login_manager.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))


class User(db.Model, UserMixin):
	__tablename__ = "users"

	id = db.Column(db.Integer, primary_key=True)
	email = db.Column(db.String(64), unique=True, index=True)
	username = db.Column(db.String(64), unique=True, index=True)
	password_hash = db.Column(db.String(128))
	is_staff = db.Column(db.Boolean)
	is_manager = db.Column(db.Boolean)

	user_details = db.relationship(
		"CardDetails", backref="details_user", lazy=True, viewonly=True)

	def __init__(self, email, username, password, is_staff, is_manager):
		self.email = email
		self.username = username
		self.password_hash = generate_password_hash(password)
		self.is_staff = is_staff
		self.is_manager = is_manager

	def check_password(self, password):
		return check_password_hash(self.password_hash, password)

	def __rep__(self):
		return f"User ID: {self.id}, Username: {self.username}"


class Movie(db.Model):
	__tablename__ = "movies"

	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(64), unique=True, index=True)
	blurb = db.Column(db.String(512), nullable=False)
	certificate = db.Column(db.String(4), nullable=False)
	actors = db.Column(db.String(128), nullable=False)
	# storing price in pence allows integers to be used and the price in £ can be found with pence/100
	price_as_pence = db.Column(db.Integer, nullable=False)
	image_path = db.Column(db.String(50))

	movie_screening = db.relationship(
		"Screening", backref="screened_movie", lazy=True, viewonly=True)

	def __init__(self, name, blurb, certificate, actors, price_as_pence, image_path):
		self.name = name
		self.blurb = blurb
		self.certificate = certificate
		self.actors = actors
		self.price_as_pence = price_as_pence
		self.image_path = image_path

	def __repr__(self):
		return f"Movie ID: {self.id}, Name: {self.name}"


class Screening(db.Model):
	__tablename__ = "screenings"

	movies = db.relationship(Movie, viewonly=True)
	id = db.Column(db.Integer, primary_key=True)
	movie_id = db.Column(db.Integer, db.ForeignKey(
		"movies.id"), nullable=False)
	time = db.Column(db.Time, nullable=False)
	date = db.Column(db.Date, nullable=False)

	screening_sale = db.relationship(
		"Sale", backref="sold_screening", lazy=True, viewonly=True)
	movie_sale = db.relationship(
		"Sale", backref="sold_movie", lazy=True, viewonly=True)

	def __init__(self, movie_id, time, date):
		self.time = time
		self.movie_id = movie_id
		self.date = date

	def __repr__(self):
		return f"Screening ID: {self.id}, Movie ID: {self.movie_id}, Time: {self.time}"


class Sale(db.Model):
	__tablename__ = "sales"
	screening = db.relationship(Screening)

	id = db.Column(db.Integer, primary_key=True)
	screening_id = db.Column(db.Integer, db.ForeignKey(
		"screenings.id"), nullable=False)
	seat = db.Column(db.Integer, nullable=False)
	price = db.Column(db.Integer, nullable=False)
	time_of_sale = db.Column(
		db.DateTime, nullable=False, default=datetime.now)

	def __init__(self, screening_id, seat, price):
		self.screening_id = screening_id
		self.seat = seat
		self.price = price


class Ticket(db.Model):
	__tablename__ = "tickets"
	screening = db.relationship(Screening)

	id = db.Column(db.Integer, primary_key=True)
	screening_id = db.Column(db.Integer, db.ForeignKey(
		"screenings.id"), nullable=False)
	seat = db.Column(db.String(8))
	email = db.Column(db.String(125))
	first_name = db.Column(db.String(125))
	last_name = db.Column(db.String(125))
	age = db.Column(db.Integer, nullable=False)

	def __init__(self, screening_id, seat, email, first_name, last_name, age):
		self.screening_id = screening_id
		self.seat = seat
		self.email = email
		self.age = age
		self.first_name = first_name
		self.last_name = last_name

	def __repr__(self):
		return f"id: {self.id}, screening_id: {self.screening_id}, email: {self.email}"


class CardDetails(db.Model):
	__tablename__ = "card_details"
	user_details = db.relationship(User, viewonly=True)
	id = db.Column(db.Integer, primary_key=True)
	user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
	card_number = db.Column(db.String(16))
	name = db.Column(db.String(64))
	expiration = db.Column(db.String(8))
	security_code = db.Column(db.String(4))
	address = db.Column(db.String(64))
	town_city = db.Column(db.String(32))
	postcode = db.Column(db.String(8))
	country = db.Column(db.String(32))

	def __init__(self, user_id, card_number, name, expiration, security_code, address, town_city, postcode, country):
		self.user_id = user_id
		self.card_number = card_number
		self.name = name
		self.expiration = expiration
		self.security_code = security_code
		self.address = address
		self.town_city = town_city
		self.postcode = postcode
		self.country = country
